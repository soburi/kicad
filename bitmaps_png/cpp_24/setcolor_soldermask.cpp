
/* Do not modify this file, it was automatically generated by the
 * PNG2cpp CMake script, using a *.png file as input.
 */

#include <bitmaps_png/bitmaps_list.h>

static const unsigned char png[] = {
 0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a, 0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
 0x00, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x18, 0x08, 0x06, 0x00, 0x00, 0x00, 0xe0, 0x77, 0x3d,
 0xf8, 0x00, 0x00, 0x03, 0x5c, 0x49, 0x44, 0x41, 0x54, 0x48, 0xc7, 0xb5, 0x95, 0x5d, 0x48, 0x53,
 0x71, 0x18, 0xc6, 0x8f, 0x5f, 0xb3, 0x0b, 0xad, 0xa0, 0x81, 0x46, 0x17, 0x0b, 0x14, 0xb2, 0x0b,
 0xc1, 0x8b, 0x2e, 0x12, 0x15, 0x74, 0x6a, 0x82, 0x41, 0x2a, 0x73, 0x6e, 0xcd, 0x0b, 0x3f, 0xd2,
 0xa3, 0xd5, 0x99, 0x73, 0x6e, 0x6d, 0x3a, 0xa7, 0x0e, 0x9b, 0x17, 0x16, 0x12, 0xa4, 0x42, 0x82,
 0x11, 0x33, 0x11, 0xe9, 0x46, 0xf0, 0x83, 0xc0, 0x2f, 0xfc, 0xf6, 0xc6, 0x0b, 0x49, 0x2d, 0x4c,
 0x24, 0x66, 0x5d, 0x28, 0x44, 0x11, 0x98, 0x09, 0x7e, 0x3c, 0xfd, 0xff, 0xc7, 0x9d, 0xb5, 0xc3,
 0x34, 0x4e, 0x56, 0x07, 0x1e, 0xc6, 0xf9, 0x9f, 0xf1, 0xfc, 0xde, 0xf3, 0xbc, 0xef, 0xbb, 0x31,
 0x2a, 0x95, 0x0a, 0xff, 0x53, 0xcc, 0x49, 0x0f, 0x74, 0x3a, 0x1d, 0x8a, 0x8b, 0x8b, 0xa1, 0xd1,
 0x68, 0xfe, 0x2d, 0xa0, 0xaa, 0xaa, 0x6a, 0xb7, 0xad, 0xad, 0xad, 0x63, 0x6e, 0x6e, 0x2e, 0x75,
 0x76, 0x76, 0x36, 0x6b, 0x74, 0x74, 0x34, 0xbb, 0xa5, 0xa5, 0x65, 0x94, 0x65, 0xd9, 0x83, 0xbf,
 0x06, 0x38, 0x1c, 0x8e, 0x8f, 0x53, 0x53, 0x53, 0xa9, 0x33, 0x33, 0x33, 0x8b, 0x04, 0x70, 0x48,
 0x04, 0xa2, 0x3d, 0x02, 0x1a, 0x18, 0x1a, 0x1a, 0xca, 0x32, 0x1a, 0x8d, 0xbb, 0xa7, 0x06, 0xd0,
 0xca, 0xa9, 0x39, 0x31, 0xdc, 0xf5, 0x18, 0x8b, 0x44, 0x20, 0x1b, 0x7d, 0x7d, 0x7d, 0xaa, 0x82,
 0x82, 0x82, 0xc3, 0x53, 0x01, 0x68, 0x2c, 0x9e, 0xca, 0x05, 0xc3, 0x77, 0xd3, 0xd3, 0xd3, 0x3d,
 0xe4, 0x73, 0x4b, 0x38, 0x23, 0xf7, 0xcf, 0x94, 0x4a, 0xe5, 0xe3, 0xd8, 0xd8, 0x58, 0x48, 0x15,
 0x23, 0x34, 0x94, 0x18, 0x29, 0x85, 0x58, 0xa8, 0xf9, 0xc2, 0xc2, 0x42, 0x08, 0x43, 0xae, 0xf9,
 0xf9, 0xf9, 0xcb, 0xe4, 0x6c, 0x47, 0x80, 0xf4, 0xf7, 0xf7, 0x4b, 0xae, 0x3e, 0x31, 0x31, 0xf1,
 0x08, 0x40, 0xa7, 0x85, 0x36, 0xd4, 0xa7, 0xd2, 0x1e, 0xc6, 0xe7, 0x22, 0x67, 0xef, 0x85, 0x67,
 0xc3, 0xc3, 0xc3, 0x92, 0x01, 0x39, 0x39, 0x39, 0x47, 0x00, 0xad, 0x56, 0x0b, 0x92, 0xff, 0x4d,
 0xda, 0x50, 0xcf, 0x1b, 0x6c, 0xd1, 0xca, 0xa9, 0x39, 0x89, 0x2d, 0x93, 0x9c, 0xed, 0x7b, 0xce,
 0xbf, 0x76, 0x75, 0x75, 0x2d, 0xa7, 0xa5, 0xa5, 0x41, 0x8a, 0xe2, 0xe2, 0xe2, 0x7e, 0xf5, 0x80,
 0x8c, 0xe2, 0x6b, 0x3a, 0x2d, 0x3e, 0x8d, 0xdd, 0x21, 0xf7, 0x6b, 0x82, 0xb9, 0x07, 0x60, 0x25,
 0x71, 0x8e, 0x25, 0x27, 0x27, 0x93, 0x58, 0x35, 0x30, 0x99, 0x4a, 0x50, 0x5b, 0x7b, 0x17, 0x16,
 0x0b, 0x4b, 0x52, 0xc8, 0x47, 0x7a, 0x7a, 0x3a, 0xe2, 0xe3, 0xe3, 0x45, 0xf2, 0x02, 0xe8, 0x9c,
 0x0f, 0x0c, 0x0c, 0x64, 0xd2, 0x69, 0x39, 0x61, 0x8a, 0xc6, 0x3b, 0x3b, 0x3b, 0x9d, 0x95, 0xdc,
 0x3d, 0x34, 0x39, 0x2d, 0x18, 0x1c, 0xcc, 0xc0, 0xfa, 0x7a, 0x14, 0x36, 0x37, 0x23, 0xe0, 0x76,
 0x2b, 0x30, 0x39, 0x99, 0x84, 0xe6, 0xe6, 0x4a, 0x70, 0xa6, 0xfb, 0x50, 0xab, 0xd5, 0xc7, 0xef,
 0x01, 0x9d, 0x73, 0x32, 0x8a, 0xd9, 0xa4, 0x07, 0x1d, 0x9e, 0xdc, 0xb7, 0xe9, 0x64, 0x11, 0x55,
 0x13, 0xf3, 0x87, 0xf5, 0xd6, 0x2a, 0xb8, 0x2c, 0xb7, 0xb1, 0xd4, 0x1e, 0x09, 0x80, 0xf1, 0x53,
 0x59, 0x5f, 0x04, 0x52, 0x9e, 0xa6, 0xc0, 0x6c, 0x37, 0x23, 0x2f, 0x2f, 0xef, 0xf8, 0x4d, 0x2e,
 0x2a, 0x2a, 0xa2, 0x0b, 0xb7, 0x4a, 0xc6, 0xf6, 0x89, 0xcb, 0xe5, 0xb2, 0xb6, 0xb6, 0xb6, 0xba,
 0xea, 0xea, 0xea, 0x3e, 0xd3, 0xca, 0xbb, 0x2c, 0x3a, 0x1c, 0xb2, 0xc4, 0x8c, 0xe8, 0xc3, 0xca,
 0x39, 0x91, 0xf9, 0xc6, 0x17, 0x19, 0x42, 0xbb, 0x65, 0x60, 0x5e, 0x31, 0x48, 0x68, 0x4f, 0x80,
 0xc1, 0x6a, 0xf8, 0xfd, 0x6f, 0x91, 0xaf, 0xe8, 0x10, 0x34, 0xd9, 0x2d, 0xd8, 0x2d, 0x3f, 0xc3,
 0x9b, 0x53, 0x7d, 0x72, 0x84, 0xe3, 0xe0, 0x20, 0xc0, 0x0b, 0x48, 0x7e, 0x29, 0xe7, 0xcd, 0x05,
 0xb1, 0x4d, 0x2c, 0x0a, 0x0b, 0x0b, 0xa5, 0x01, 0xca, 0xd9, 0x52, 0x0c, 0x9a, 0x32, 0xbc, 0xe6,
 0x82, 0x56, 0xc6, 0xe5, 0xbc, 0xf9, 0xc8, 0x6a, 0x18, 0x02, 0x7a, 0x03, 0x44, 0x80, 0x98, 0xe7,
 0x31, 0xe0, 0x1e, 0x70, 0xd2, 0x00, 0x36, 0x23, 0x87, 0x75, 0x7d, 0x94, 0x1f, 0xe0, 0x9b, 0x51,
 0x86, 0xef, 0xdb, 0x21, 0x88, 0x7e, 0x71, 0x5e, 0x64, 0x4e, 0x15, 0xda, 0x1b, 0x0a, 0x6b, 0x83,
 0x55, 0x1a, 0xa0, 0xc1, 0x64, 0xc0, 0xbc, 0xee, 0x0a, 0xde, 0xe6, 0xc8, 0xfd, 0xe4, 0xac, 0xb8,
 0x01, 0xb9, 0xcd, 0x76, 0xac, 0x6c, 0xf5, 0xf5, 0xd2, 0x00, 0x76, 0x8e, 0x83, 0x5b, 0xa1, 0x20,
 0x69, 0x30, 0x22, 0xfd, 0x08, 0x0c, 0x85, 0x36, 0x6c, 0x0c, 0x91, 0xb9, 0x1b, 0x60, 0x58, 0x88,
 0x14, 0x54, 0xb6, 0x8f, 0x6a, 0x7b, 0xa3, 0x34, 0x80, 0xbe, 0xac, 0x0c, 0x93, 0x49, 0x49, 0x7e,
 0x00, 0xa7, 0xbc, 0x06, 0x6a, 0x66, 0x19, 0xb7, 0x2e, 0x8d, 0xf9, 0x01, 0x14, 0x7a, 0x37, 0x2a,
 0xcc, 0x12, 0xdf, 0x20, 0x3f, 0x3f, 0x1f, 0x8f, 0x2a, 0x2b, 0x71, 0x10, 0x18, 0xe8, 0x35, 0x77,
 0xcb, 0x2e, 0x42, 0x1d, 0xbc, 0xc0, 0x03, 0xa8, 0xae, 0xa6, 0x2f, 0x8a, 0x00, 0x1a, 0x4b, 0x37,
 0x4a, 0x4a, 0x4a, 0xa5, 0x01, 0xa8, 0xac, 0x24, 0xa6, 0x91, 0x94, 0x14, 0x2f, 0x80, 0xbb, 0xd0,
 0xe1, 0x35, 0xa7, 0x52, 0x85, 0xcf, 0x20, 0xb8, 0x64, 0x8f, 0x37, 0x8f, 0xd6, 0xaf, 0xc1, 0x5c,
 0xe3, 0x40, 0x6e, 0x6e, 0xae, 0x74, 0x00, 0x5d, 0x7f, 0x87, 0xd9, 0x8c, 0xa9, 0x84, 0x04, 0x4c,
 0x9c, 0xbd, 0x0e, 0x75, 0xc0, 0x92, 0x08, 0x40, 0x15, 0x7f, 0x6d, 0x92, 0x37, 0xb7, 0xd8, 0x9d,
 0xfc, 0x5f, 0x80, 0xe4, 0x45, 0x13, 0x44, 0xd7, 0xdf, 0x66, 0x30, 0xa0, 0xa6, 0xbc, 0x09, 0x77,
 0x62, 0x86, 0xa1, 0x91, 0x2d, 0xf2, 0xc6, 0x79, 0x41, 0x6f, 0x50, 0xa8, 0x98, 0x80, 0x41, 0xdb,
 0xce, 0x57, 0x2e, 0x98, 0xff, 0x31, 0x40, 0x10, 0xdd, 0x50, 0x23, 0x57, 0x8d, 0x5a, 0xab, 0x03,
 0x75, 0x35, 0x8d, 0xa8, 0xad, 0x76, 0xc0, 0x5c, 0x61, 0xe7, 0x33, 0xa7, 0xb1, 0xf8, 0x7e, 0xf7,
 0x27, 0x4d, 0x2a, 0xf2, 0x0b, 0x46, 0x5c, 0x35, 0xc6, 0x00, 0x00, 0x00, 0x00, 0x49, 0x45, 0x4e,
 0x44, 0xae, 0x42, 0x60, 0x82,
};

const BITMAP_OPAQUE setcolor_soldermask_xpm[1] = {{ png, sizeof( png ), "setcolor_soldermask_xpm" }};

//EOF
